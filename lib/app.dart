import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'src/widget_list.dart';

class WidgetApp extends StatelessWidget {
  @override
  Widget build( context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        sliderTheme: SliderThemeData(
          activeTrackColor: Colors.red,
          inactiveTrackColor: Colors.yellow,
        ),
      ),
      supportedLocales: [
        Locale('en'),
        Locale('es'),
      ],
      locale: Locale('es'),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      home: WidgetList(),
    );
  }
}
