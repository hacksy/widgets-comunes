import 'package:flutter/material.dart';

class CheckboxSwitchWidget extends StatefulWidget {
  @override
  _CheckboxSwitchWidgetState createState() => _CheckboxSwitchWidgetState();
}

class _CheckboxSwitchWidgetState extends State<CheckboxSwitchWidget> {
  bool _defaultCheckbox = true;
  bool _defaultListTileCheckbox = true;

  bool _defaultSwitch = true;
  bool _defaultListTileSwitch = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Checkbox(
                value: _defaultCheckbox,
                onChanged: (boolean) {
                  setState(() {
                    _defaultCheckbox = boolean;
                  });
                },
              ),
              Switch(
                value: _defaultSwitch,
                onChanged: (boolean) {
                  setState(() {});
                  _defaultSwitch = boolean;
                },
              ),
              ListTile(
                title: Text("title"),
                subtitle: Text("subtitle"),
                leading: Icon(Icons.add),
                trailing: Icon(Icons.mic),
              ),
              CheckboxListTile(
                value: _defaultListTileCheckbox,
                title: Text("title"),
                subtitle: Text("subtitle"),
                onChanged: (boolean) {
                  setState(() {});
                  _defaultListTileCheckbox = boolean;
                },
              ),
              SwitchListTile(
                value: _defaultListTileSwitch,
                title: Text("title"),
                subtitle: Text("subtitle"),
                onChanged: (boolean) {
                  setState(() {});
                  _defaultListTileSwitch = boolean;
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
