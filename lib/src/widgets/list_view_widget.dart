import 'package:flutter/material.dart';

class ListViewWidget extends StatefulWidget {
  @override
  _ListViewWidgetState createState() => _ListViewWidgetState();
}

class _ListViewWidgetState extends State<ListViewWidget> {
  var list = <String>[];
  ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    list = List<String>.generate(15, (index) => 'Text $index');
    _scrollController.addListener(() {
      int initial = list.length;
      if (_scrollController.position.maxScrollExtent <=
          _scrollController.offset) {
        list.addAll(
            List<String>.generate(15, (index) => 'Text ${initial + index}'));
        setState(() {});
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Widget List"),
      ),
      body: Align(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.red,
            shape: BoxShape.circle,
          ),
          child: RefreshIndicator(
            onRefresh: () async{
              await Future.delayed(Duration(seconds: 5));
              print("Updating");
            },
            child: ListView.separated(
              controller: _scrollController,
              itemCount: list.length,
              separatorBuilder: (BuildContext context, int pos) {
                return Divider(
                  height: 3,
                  thickness: 3,
                );
              },
              itemBuilder: (BuildContext context, int pos) {
                return Text(list[pos], style: TextStyle(fontSize: 35));
              },
            ),
          ),
        ),
      ),
    );
  }
}
