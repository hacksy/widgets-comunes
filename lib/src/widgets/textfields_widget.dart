import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldsWidget extends StatefulWidget {
  @override
  _TextFieldsWidgetState createState() => _TextFieldsWidgetState();
}

class _TextFieldsWidgetState extends State<TextFieldsWidget> {
  bool _hidePassword = true;
  final _controller = TextEditingController();
  FocusNode _focusNode = FocusNode();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    _controller.addListener(() {
      print(_controller.text);
      /* if (_controller.text.length > 5) {
        _controller.text = _controller.text.substring(0, 4);
      }*/
    });
    super.initState();
  }

  @override
  Widget build(context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            width: 200,
            child: Column(
              children: [
                TextField(
                  autofocus: true,
                  maxLines: 5,
                  autocorrect: true,
                  controller: _controller,
                  obscureText: false,
                  //holamundo(contexto, maxLength:25,currentLength:12, isFocusedtrue);
                  buildCounter: (context,
                      {currentLength, maxLength, isFocused}) {
                    return Text("Has escrito $currentLength de $maxLength");
                  },
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.go,
                  inputFormatters: [
                    //LengthLimitingTextInputFormatter(5),
                    FilteringTextInputFormatter.deny(RegExp(r'[0-9]')),
                  ],

                  onSubmitted: (string) {
                    print(string);
                    _focusNode.requestFocus();
                  },
                  onChanged: (string) {
                    // print(string);
                  },
                  maxLengthEnforced: true,
                  maxLength: 25,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    errorText: 'Mensaje de error',
                    labelText: "Ingresa su usuario",
                    hintStyle: TextStyle(color: Colors.red, fontSize: 8),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                TextField(
                  focusNode: _focusNode,
                  obscureText: _hidePassword,
                  decoration: InputDecoration(
                    hintText: 'Ingresa su contrasenia',
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() => _hidePassword = !_hidePassword);
                      },
                      child: Icon(_hidePassword
                          ? Icons.remove_red_eye
                          : Icons.remove_red_eye_outlined),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Form(
                  key: _formKey,
                  child: TextFormField(
                    validator: (text){
                      if(text.length<10){
                        return "Error here";
                      }
                      return null;
                    },
                    autofocus: true,
                  //  maxLines: 5,
                    autocorrect: true,
                    controller: _controller,
              //    obscureText: false,
                    //holamundo(contexto, maxLength:25,currentLength:12, isFocusedtrue);
                   /* buildCounter: (context,
                        {currentLength, maxLength, isFocused}) {
                      return Text("Has escrito $currentLength de $maxLength");
                    },*/
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.go,
                    inputFormatters: [
                      //LengthLimitingTextInputFormatter(5),
                    //  FilteringTextInputFormatter.deny(RegExp(r'[0-9]')),
                    ],

                    onSaved: (string) {
                      print(string);
                      _focusNode.requestFocus();
                    },
                    onChanged: (string) {
                      // print(string);
                    },
                    maxLengthEnforced: true,
                    maxLength: 25,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      labelText: "Ingresa su usuario",
                      hintStyle: TextStyle(color: Colors.red, fontSize: 8),
                    ),
                  ),
                ),
                RaisedButton(onPressed: (){
                 if( _formKey.currentState.validate()){
                   print("Okay");
                 }else{
                   print("Error");
                 }
                },

                child: Text("Validar"),),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

