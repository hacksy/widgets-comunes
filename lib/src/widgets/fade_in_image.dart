import 'package:flutter/material.dart';

class FadeInImageWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
              padding: EdgeInsets.all(9),
              color: Colors.red,
              child: FadeInImage.assetNetwork(
                  placeholder: 'assets/dart.png',
                  image:
                      'https://flutter.dev/images/flutter-logo-sharing.png')),
        ),
      ),
    );
  }
}
