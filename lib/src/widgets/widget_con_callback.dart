import 'package:flutter/material.dart';

class CallbackWidget extends StatelessWidget{
  CallbackWidget({this.text, this.onPressed});
  final String text;
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context){
    return RaisedButton(
      child: Text(text),
      onPressed: onPressed,
    );
  }
}

class ParentWidget extends StatefulWidget{
  @override
  _ParentWidgetState createState() => _ParentWidgetState();
}

class _ParentWidgetState extends State<ParentWidget> {
  var _text = '';

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            TextField(
              onChanged: (string){
                _text = string;
              },
            ),
            Text(_text),
            CallbackWidget(text:'Actualizar Texto',
            onPressed: (){
              setState(() {

              });
            },),
          ],
        ),
      ),
    );
  }
}