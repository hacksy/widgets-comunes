import 'package:flutter/material.dart';

class SliderWidget extends StatefulWidget {
  @override
  _SliderWidgetState createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  var _currentValue = 35.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Slider(
            onChanged: (value) {
              _currentValue = value;
              setState(() {});
            },
            value: _currentValue,
            min: 0,
            max: 100,
          ),
        ),
      ),
    );
  }
}
