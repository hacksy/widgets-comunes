import 'package:flutter/material.dart';

class CircleAvatarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
              padding: EdgeInsets.all(9),
              //  child: Image.network("https://dartpad.dev/dart-192.png")),
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/dart.png'),
                  backgroundColor: Colors.red,)),
        ),
      ),
    );
  }
}