import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedContainerWidget extends StatefulWidget {
  @override
  _AnimatedContainerWidgetState createState() =>
      _AnimatedContainerWidgetState();
}

class _AnimatedContainerWidgetState extends State<AnimatedContainerWidget> {
  var _color = Colors.red[300];

  var _width = 40.0;

  var _height = 40.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          final random = Random();
          _width = random.nextInt(350).toDouble();
          _height = random.nextInt(350).toDouble();
          _color = Color.fromRGBO(
              random.nextInt(256), random.nextInt(256), random.nextInt(256), 1);
          setState(() {

          });
        },
        child: Icon(Icons.add),
      ),
      body: Center(
        child: AnimatedContainer(
          curve: Curves.ease,
          color: _color,
          width: _width,
          duration: Duration(seconds: 2),
          height: _height,
        ),
       /* child: Container(
          color: _color,
          width: _width,
          height: _height,
        ),*/
      ),
    );
  }
}
