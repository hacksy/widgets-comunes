import 'package:flutter/material.dart';

class ImageWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
              padding: EdgeInsets.all(9),
              color: Colors.red,
            //  child: Image.network("https://dartpad.dev/dart-192.png")),
              child: Image.asset('assets/dart.png')),
        ),
      ),
    );
  }
}
