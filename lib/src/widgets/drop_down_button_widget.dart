import 'package:flutter/material.dart';

class DropDownButtonWidget extends StatefulWidget {
  @override
  _DropDownButtonWidgetState createState() => _DropDownButtonWidgetState();
}

class _DropDownButtonWidgetState extends State<DropDownButtonWidget> {
  var _currentIndex = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: DropdownButton(
            value: _currentIndex,
            onChanged: (index) {
              _currentIndex = index;
              setState(() {});

            },
            items: [
              DropdownMenuItem(
                child: Text("Test"),
                value: 1,
              ),
              DropdownMenuItem(
                child: Text("Test 2"),
                value: 2,
              ),
              DropdownMenuItem(
                child: Text("Test 3"),
                value: 3,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
