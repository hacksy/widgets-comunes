import 'package:flutter/material.dart';
import 'package:pedantic/pedantic.dart';
import 'package:widgets_comunes/src/widgets/animated_container_widget.dart';
import 'package:widgets_comunes/src/widgets/circle_avatar_dart.dart';
import 'package:widgets_comunes/src/widgets/drop_down_button_widget.dart';
import 'package:widgets_comunes/src/widgets/fade_in_image.dart';
import 'package:widgets_comunes/src/widgets/list_view_widget.dart';
import 'package:widgets_comunes/src/widgets/textfields_widget.dart';
import 'package:widgets_comunes/src/widgets/widget_con_callback.dart';
import 'widgets/card_widget.dart';
import 'widgets/checkbox_switch_widget.dart';
import 'widgets/image_widget.dart';
import 'widgets/slider_widget.dart';

class WidgetList extends StatelessWidget {
  @override
  Widget build(context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Widget List'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ChangeScreenButton(
                text: 'Ir a Widget con Callback', widget: ParentWidget()),
            ChangeScreenButton(
                text: 'Ir a Widget TextBox', widget: TextFieldsWidget()),
            ChangeScreenButton(
                text: 'Ir a Widget CheckBox - Switch',
                widget: CheckboxSwitchWidget()),
            ChangeScreenButton(
                text: "Ir a Widget Lista + Scroll infinito + pull to refresh",
                widget: ListViewWidget()),
            ChangeScreenButton(text: "Ir a Widget Card", widget: CardWidget()),
            ChangeScreenButton(
                text: "Ir a Widget Image", widget: ImageWidget()),
            ChangeScreenButton(
                text: "Ir a Widget FadeInImage", widget: FadeInImageWidget()),
            ChangeScreenButton(
                text: "Ir a Widget CircleAvatar", widget: CircleAvatarWidget()),
            RaisedButton(
              onPressed: () async {
                unawaited(showDatePicker(
                  context: context,
                  initialDate: DateTime.parse("2021-01-01"),
                  firstDate: DateTime.parse("2020-01-03"),
                  lastDate: DateTime.parse("2021-02-25"), //asdasd
                ));
              },
              child: Text("DatePicker"),
            ),
            RaisedButton(
              onPressed: () async {
                unawaited(showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text("Title"),
                        content: Text("Content"),
                        actions: [
                          FlatButton(onPressed: null, child: Text('Aceptar')),
                          FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('Cancelar')),
                        ],
                      );
                    }));
              },
              child: Text("AlertDialog"),
            ),
            ChangeScreenButton(
                text: "Ir a Widget DropDown", widget: DropDownButtonWidget()),
            ChangeScreenButton(
                text: "Ir a Widget Slider", widget: SliderWidget()),
            ChangeScreenButton(
                text: "Ir a Widget AnimatedContainer",
                widget: AnimatedContainerWidget()),
          ],
        ),
      ),
    );
  }
}

class ChangeScreenButton extends StatelessWidget {
  ChangeScreenButton({this.text, this.widget});
  final String text;
  final Widget widget;
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () => _onPressed(context, widget),
      child: Text(text),
    );
  }

  void _onPressed(BuildContext context, Widget widget) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return widget;
    }));
  }
}
